#ifndef LISTREQUEST_HPP
#define LISTREQUEST_HPP

#include "Request.hpp"

namespace Hammerspace
{
class ListRequest : public Request
{
	public:
		RequestPB Build ();
		void Execute (ip::tcp::socket& sock);
	protected:
	private:
};
}
#endif // LISTREQUEST_HPP
