#include "Client.hpp"
#include "functions.hpp"
#include "PutRequest.hpp"
#include "GetRequest.hpp"
#include "RemoveRequest.hpp"
#include "ListRequest.hpp"

namespace Hammerspace
{

Client::Client(po::variables_map& vm) :
	host (vm["host"].as<std::string>()),
	port ("45000"),
	resolver (io_service),
	sock (io_service),
	list_needed (vm.count("list")),
	requests (new std::vector<Request*>)
{
	if (vm.count("port"))
	{
		port = vm["port"].as<std::string>();
	}
	if (vm.count("put"))
	{
		putfiles = vm["put"].as< std::vector<std::string> >();
		requests->push_back(new PutRequest(putfiles));
	}
	if (vm.count("get"))
	{	
		get_ids = vm["get"].as< std::vector<unsigned long> >();
		requests->push_back(new GetRequest(get_ids));
	}
	if (vm.count("remove"))
	{
		remove_ids = vm["remove"].as< std::vector<unsigned long> >();
		requests->push_back(new RemoveRequest(remove_ids));
	}
	if (list_needed)
	{
		requests->push_back(new ListRequest());
	}

	std::cout << "Connecting to "<< host << ":" << port <<".\n";
	asio::connect(sock, resolver.resolve( {host, port} ));
	std::cout << "Connected to "<< host << ":" << port <<".\n";
}

void Client::run()
{
	for (auto iter = requests->begin(); iter != requests->end(); iter++)
	{
		(*iter)->Execute(sock);
	}
}

}