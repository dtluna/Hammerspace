#ifndef REMOVEREQUEST_HPP
#define REMOVEREQUEST_HPP

#include "Request.hpp"

namespace Hammerspace
{
class RemoveRequest : public Request
{
	public:
		RemoveRequest (std::vector<unsigned long>& ids);
		RequestPB Build (unsigned long& id);
		void Execute (ip::tcp::socket& sock);

	protected:
	private:
		std::vector<unsigned long> ids;
};
}
#endif // REMOVEREQUEST_HPP
