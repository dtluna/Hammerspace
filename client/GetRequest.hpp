#ifndef GETREQUEST_HPP
#define GETREQUEST_HPP

#include "Request.hpp"

namespace Hammerspace
{
class GetRequest : public Request
{
	public:
		GetRequest (std::vector<unsigned long>& ids);
		RequestPB Build (unsigned long& id);
		void Execute (ip::tcp::socket& sock);
		void read_file (ip::tcp::socket& sock, std::string& filename, size_t& filesize);
	protected:
	private:
		std::vector<unsigned long> ids;
};
}
#endif // GETREQUEST_HPP
