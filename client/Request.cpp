#include "Request.hpp"

namespace Hammerspace
{

void Request::send (ip::tcp::socket& sock, size_t& request_length)
{
	asio::write(sock, asio::buffer(&request_length, sizeof(request_length)));//пишем в сокет длину запроса
}

void Request::send (ip::tcp::socket& sock, RequestPB& request)
{
	asio::streambuf buf; //создаём буфер
	std::ostream os(&buf); //создаём поток вывода для буфера
	request.SerializeToOstream(&os);//сериализуем сообщение в поток вывода
	asio::write(sock, buf); //пишем в сокет из буфера
}

void Request::read (ip::tcp::socket& sock, ReplyPB& reply)
{
	boost::system::error_code error;
	size_t message_length;
	sock.read_some(asio::buffer(&message_length, sizeof(size_t)), error);//считываем длину сообщения
	if (error)
		throw boost::system::system_error(error); // Some other error.

	char* data = new char[message_length]; //массив для считывания данных из сокета
	sock.read_some(asio::buffer(data, message_length), error);//синхронно читаем данные в буфер
	if (error)
		throw boost::system::system_error(error); // Some other error.

	reply.ParseFromArray (data, message_length);
	delete [] data;
}

}