#include "GetRequest.hpp"
namespace Hammerspace
{

GetRequest::GetRequest (std::vector<unsigned long>& ids) :
	ids(ids)
{}

void GetRequest::read_file (ip::tcp::socket& sock, std::string& filename, size_t& filesize)
{
	boost::system::error_code error;
	std::ofstream file (filename, std::ios::out |std::ios::binary);//открыли файл для записи в него
	char* data = new char[filesize]; //массив для считывания данных из сокета

	sock.read_some(asio::buffer(data, filesize), error);//синхронно читаем данные в буфер
	if (error)
		throw boost::system::system_error(error); // Some other error.

	file.write(data, filesize);
	file.close();
}

RequestPB GetRequest::Build (unsigned long& id)
{
	RequestPB request;
	GetRequestPB* GR = request.MutableExtension(GetRequestPB::get_request);

	GR->set_id(id);

	return request;
}

void GetRequest::Execute (ip::tcp::socket& sock)
{
	for (int i = 0; i < ids.size(); i++)
	{
		RequestPB request = Build (ids[i]);
		size_t request_length = request.ByteSize();
		send (sock, request_length);
		send (sock, request);
		ReplyPB reply;
		read (sock, reply);
		if (true == reply.success())
		{
			std::cout << "Operation is successful.\n";
			size_t filesize = reply.filesize(i);
			std::string filename = reply.filename(i);
			read_file (sock, filename, filesize);
			std::cout << "Filepath: " << reply.filename(i) << "\n";
		}
		else
		{
			std::cout << "Operation is not successful.\n";
		}
	}
}
}
