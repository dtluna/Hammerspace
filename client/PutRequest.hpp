#ifndef PUTREQUEST_HPP
#define PUTREQUEST_HPP

#include "Request.hpp"

namespace Hammerspace
{
class PutRequest : public Request
{
	public:
		PutRequest(std::vector<std::string>& putfiles);
		RequestPB Build (std::string& filename, size_t& filesize);
		void Execute (ip::tcp::socket& sock);
		void send_file (ip::tcp::socket& sock, std::string& filename, size_t& filesize);
	protected:
	private:
		std::vector<std::string> putfiles;
};
}
#endif // PUTREQUEST_HPP
