#ifndef CLIENT_HPP
#define CLIENT_HPP

#include "functions.hpp"
#include "Request.hpp"

namespace Hammerspace
{

class Client
{
public:
	Client(po::variables_map& vm);
	void run();
private:
	std::string host;
	std::string port;
	asio::io_service io_service;
	ip::tcp::resolver resolver;
	ip::tcp::socket sock;
	std::vector<std::string> putfiles;
	std::vector<unsigned long> get_ids;
	std::vector<unsigned long> remove_ids;
	bool list_needed;
	std::shared_ptr < std::vector<Request*> > requests;
};

}

#endif // CLIENT_HPP