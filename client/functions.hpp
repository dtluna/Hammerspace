#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <boost/program_options.hpp>
#include <boost/asio.hpp>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include "Hammerspace.pb.h"
#include <sys/sendfile.h>
#include <boost/filesystem.hpp>

namespace po = boost::program_options;
namespace ip = boost::asio::ip;
namespace asio = boost::asio;
namespace protobuf = google::protobuf;
namespace filesystem = boost::filesystem;

namespace Hammerspace
{

void help (po::options_description& desc);

}
#endif // FUNCTIONS_HPP
