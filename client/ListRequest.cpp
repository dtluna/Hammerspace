#include "ListRequest.hpp"

namespace Hammerspace
{

RequestPB ListRequest::Build ()
{
	RequestPB request;
	ListRequestPB* LR = request.MutableExtension(ListRequestPB::list_request);

	return request;
}

void ListRequest::Execute (ip::tcp::socket& sock)
{
	
	RequestPB request = Build ();
	size_t request_length = request.ByteSize();
	send (sock, request_length);
	send (sock, request);
	ReplyPB reply;
	read (sock, reply);
	if (true == reply.success())
	{
		std::cout << "Operation is successful.\n";
		for (int i = 0; i < reply.id_size(); ++i)
		{
			std::cout << "Filename: " << reply.filename(i)
			<< "; ID: " << reply.id(i) << "; Size: " << reply.filesize(i) << "\n";
		}
	}
	else
	{
		std::cout << "Operation is not successful.\n";
	}

}
}
