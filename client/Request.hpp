#ifndef REQUEST_HPP
#define REQUEST_HPP

#include "functions.hpp"

namespace Hammerspace
{
class Request
{
	public:
		virtual void Execute (ip::tcp::socket& sock) = 0;
		void send (ip::tcp::socket& sock, size_t& request_length);
		void send (ip::tcp::socket& sock, RequestPB& Request);
		void read (ip::tcp::socket& sock, ReplyPB& reply);

	protected:
	private:
};
}
#endif // REQUEST_HPP
