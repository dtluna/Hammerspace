#include "RemoveRequest.hpp"

namespace Hammerspace
{

RemoveRequest::RemoveRequest (std::vector<unsigned long>& ids) :
	ids(ids)
{}

RequestPB RemoveRequest::Build (unsigned long& id)
{
	RequestPB request;
	RemoveRequestPB* RR = request.MutableExtension(RemoveRequestPB::remove_request);

	RR->set_id(id);

	return request;
}

void RemoveRequest::Execute (ip::tcp::socket& sock)
{
	for (int i = 0; i < ids.size(); i++)
	{
		RequestPB request = Build (ids[i]);
		size_t request_length = request.ByteSize();
		send (sock, request_length);
		send (sock, request);
		ReplyPB reply;
		read (sock, reply);
		if (true == reply.success())
		{
			std::cout << "Operation is successful.\n";
		}
		else
		{
			std::cout << "Operation is not successful.\n";
		}
	}
}
}
