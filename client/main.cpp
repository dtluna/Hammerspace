#include "Client.hpp"

using namespace Hammerspace;

int main(int argc, char *argv[])
{
	try
	{
		// Verify that the version of the library that we linked against is
		// compatible with the version of the headers we compiled against
		GOOGLE_PROTOBUF_VERIFY_VERSION;

		po::options_description desc("Allowed options");
		desc.add_options()
		("help,h", "produce help message")
		("host", po::value< std::string >(),
			"host to connect")
		("port", po::value< std::string >(),
			"port to connect")
		("put,p", po::value< std::vector<std::string> >(),
			"put <filename> to storage")
		("get,g", po::value< std::vector<unsigned long> >(),
			"get <id> from storage and save it")
		("remove,r", po::value< std::vector<unsigned long> >(),
			"remove <id> from storage")
		("list,l", "list all objects in storage")
		;

		po::positional_options_description p;
		p.add("host", -1);

		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).
			options(desc).positional(p).run(), vm);
		po::notify(vm);

		asio::io_service io_service;
		ip::tcp::socket sock(io_service);

		if (vm.count("help"))
		{
			help(desc);
		}

		if (!vm.count("host"))
		{
			std::cerr << "Usage: client [--host] <host>.\n";
			return 1;
		}		

		Client client(vm);
		client.run();

		vm.clear();
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}
	return 0;
}
