#include "PutRequest.hpp"

namespace Hammerspace
{

PutRequest::PutRequest(std::vector<std::string>& putfiles) :
	putfiles(putfiles)
{}

RequestPB PutRequest::Build (std::string& filename, size_t& filesize)
{
	RequestPB request;
	PutRequestPB* PR = request.MutableExtension(PutRequestPB::put_request);

	PR->set_filename(filename);
	PR->set_filesize(filesize);

	return request;
}

void PutRequest::send_file (ip::tcp::socket& sock, std::string& filename, size_t& filesize)
{	
	std::ifstream file (filename, std::ios::binary);
	char* raw_file = new char [filesize];
	
	file.read(raw_file, filesize);
	auto bytes_written = asio::write(sock, asio::buffer(raw_file, filesize)); //пишем в сокет из буфера
	std::cout << "Bytes written = " << bytes_written << "\n";
}

void PutRequest::Execute (ip::tcp::socket& sock)
{
	for (int i = 0; i < putfiles.size(); i++)
	{
		size_t filesize = filesystem::file_size(putfiles[i]);
		RequestPB request = Build (putfiles[i], filesize);

		size_t request_length = request.ByteSize();
		send (sock, request_length);
		send (sock, request);
		send_file (sock, putfiles[i], filesize);
		
		ReplyPB reply;
		read (sock, reply);
		if (true == reply.success())
		{
			std::cout << "Operation is successful.\n";
			std::cout << "ID: " << reply.id(i) << "\n";
		}
		else
		{
			std::cout << "Operation is not successful.\n";
		}
	}	
}

}
