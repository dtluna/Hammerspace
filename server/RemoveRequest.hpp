#ifndef REMOVEREQUEST_HPP
#define REMOVEREQUEST_HPP

#include "Request.hpp"

namespace Hammerspace
{
class RemoveRequest : public Request
{
	public:
		bool CanHandle (RequestPB& request);
		void Handle (ip::tcp::socket sock, RequestPB& request,
			std::shared_ptr < std::map <unsigned long, ObjectAdress> > storage);

	protected:
	private:
};
}
#endif // REMOVEREQUEST_HPP