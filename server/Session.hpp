#ifndef SESSION_HPP
#define SESSION_HPP

#include "functions.hpp"
#include "Server.hpp"

namespace Hammerspace
{

class Session
{
public:
	Session(ip::tcp::socket  sock, std::shared_ptr <Server> server);
	void run();
	RequestPB receive_request ();
private:
    ip::tcp::socket sock;
	std::shared_ptr <Server> server;
};

}

#endif // SESSION_HPP
