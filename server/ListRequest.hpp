#ifndef LISTREQUEST_HPP
#define LISTREQUEST_HPP

#include "Request.hpp"

namespace Hammerspace
{
class ListRequest : public Request
{
	public:
		bool CanHandle (RequestPB& request);
		void Handle (ip::tcp::socket sock, RequestPB& request,
			std::shared_ptr < std::map <unsigned long, ObjectAdress> > storage);

	protected:
	private:
};
}
#endif // LISTREQUEST_HPP