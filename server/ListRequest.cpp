#include "ListRequest.hpp"

namespace Hammerspace
{

bool ListRequest::CanHandle (RequestPB& request)
{
	if(request.HasExtension(ListRequestPB::list_request) )
		return true;
	else return false;
}

void ListRequest::Handle (ip::tcp::socket sock, RequestPB& request,
	std::shared_ptr < std::map <unsigned long, ObjectAdress> > storage)
{
	ReplyPB reply;

	for (auto iter = storage->begin(); iter != storage->end(); iter++)
	{
		unsigned long key = iter->first;
		ObjectAdress value = iter->second;

		reply.add_id(key);
		reply.add_filename(value.get_filename());
		reply.add_filesize(value.get_size());
	}
	reply.set_success(true);

	size_t reply_length = reply.ByteSize();//записываем длину запроса
	size_t prefix_length = sizeof(reply_length);//вычисляем длину префикса (длины запроса)
	asio::write(sock, asio::buffer(&reply_length, prefix_length));//пишем в сокет длину запроса

	asio::streambuf buf; //создаём буфер
	std::ostream os(&buf); //создаём поток вывода для буфера
	reply.SerializeToOstream(&os);//сериализуем ответ в поток вывода
	asio::write(sock, buf); //пишем в сокет из буфера
}
}
