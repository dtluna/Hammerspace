#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <map>
#include <boost/program_options.hpp>
#include <boost/asio.hpp>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <thread>
#include <utility>
#include <boost/threadpool.hpp>
#include "Hammerspace.pb.h"


namespace po = boost::program_options;
namespace ip = boost::asio::ip;
namespace asio = boost::asio;
namespace protobuf = google::protobuf;
namespace threadpool = boost::threadpool;

namespace Hammerspace
{

enum REQUEST_TYPE
{
	PUT = 1,
	GET = 2,
	REMOVE = 3,
	LIST = 4
};

void help (po::options_description& desc);
}
#endif // FUNCTIONS_HPP
