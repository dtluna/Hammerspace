#include "GetRequest.hpp"

namespace Hammerspace
{

bool GetRequest::CanHandle (RequestPB& request)
{
	if(request.HasExtension(GetRequestPB::get_request) )
		return true;
	else return false;
}

void GetRequest::Handle (ip::tcp::socket sock, RequestPB& request,
	std::shared_ptr < std::map <unsigned long, ObjectAdress> > storage)
{
	GetRequestPB* get_request = request.MutableExtension(GetRequestPB::get_request);

	unsigned long id = get_request->id();
	ReplyPB reply;

	auto iter = storage->find(id);
	if (iter == storage->end())//нет элемента с таким ID
	{
		reply.set_success(false);

		size_t reply_length = reply.ByteSize();//записываем длину запроса
		size_t prefix_length = sizeof(reply_length);//вычисляем длину префикса (длины запроса)
		asio::write(sock, asio::buffer(&reply_length, prefix_length));//пишем в сокет длину запроса

		asio::streambuf buf; //создаём буфер
		std::ostream os(&buf); //создаём поток вывода для буфера
		reply.SerializeToOstream(&os);//сериализуем ответ в поток вывода
		asio::write(sock, buf); //пишем в сокет из буфера
	}
	else
	{
		ObjectAdress Meta = (*storage)[id];

		auto filesize = Meta.get_size();
		auto offset = Meta.get_offset();
		auto filename = Meta.get_filename();

		char* data = new char[filesize];

		auto fd = open("storage.str", O_RDWR, 0660);

		pread (fd, data, filesize, offset);

		reply.add_filename(filename);
		reply.add_filesize(filesize);
		reply.set_success(true);

		size_t reply_length = reply.ByteSize();//записываем длину запроса
		size_t prefix_length = sizeof(reply_length);//вычисляем длину префикса (длины запроса)
		asio::write(sock, asio::buffer(&reply_length, prefix_length));//пишем в сокет длину запроса

		asio::streambuf buf; //создаём буфер
		std::ostream os(&buf); //создаём поток вывода для буфера
		reply.SerializeToOstream(&os);//сериализуем ответ в поток вывода
		asio::write(sock, buf); //пишем в сокет из буфера

		asio::write(sock, asio::buffer(data, filesize));
	}
}

}
