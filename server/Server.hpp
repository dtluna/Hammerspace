#ifndef SERVER_HPP
#define SERVER_HPP

#include "functions.hpp"
#include "ObjectAdress.hpp"
#include "Request.hpp"

namespace Hammerspace
{

class Server
{
public:
	Server (asio::io_service& io, unsigned short int port, int cp_num);
	void run ();
	std::shared_ptr < std::map <unsigned long, ObjectAdress> > get_storage ();
	std::shared_ptr < std::vector<Request*> >  get_dispatchers ();

private:
	std::shared_ptr < std::map <unsigned long, ObjectAdress> > storage;
	std::shared_ptr < std::vector<Request*> > dispatchers;
	std::shared_ptr <threadpool::pool> connection_pool;
    unsigned short port;
    ip::tcp::acceptor a;
	ip::tcp::socket sock;
};

}
#endif // SERVER_HPP
