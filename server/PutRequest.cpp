#include "PutRequest.hpp"

namespace Hammerspace
{

bool PutRequest::CanHandle (RequestPB& request)
{
	if(request.HasExtension(PutRequestPB::put_request) )
		return true;
	else return false;
}

void PutRequest::Handle (ip::tcp::socket sock, RequestPB& request,
	std::shared_ptr < std::map <unsigned long, ObjectAdress> > storage)
{

	PutRequestPB* put_request = request.MutableExtension(PutRequestPB::put_request);

	std::string filename = put_request->filename();
	unsigned long filesize = put_request->filesize();

	boost::system::error_code error;
	char* data = new char[filesize]; //массив для считывания данных из сокета
	size_t bytes_read = 0;
	//bytes_read +=asio::read(sock, asio::buffer(data, filesize), error);
	bytes_read += sock.read_some(asio::buffer(data, filesize), error);//синхронно читаем данные в буфер
	std::cout << "Bytes read = " << bytes_read << "\n"
	<< "When size of file = " << filesize << " bytes.\n";
	if (error)
		throw boost::system::system_error(error); // Some other error.

	std::hash<std::string> hash_fn;
	unsigned long id = hash_fn(filename);//посчитали хэш

	auto fd = open("storage.str", O_RDWR, 0660);

	auto iter = storage->find(id);
	if (iter == storage->end())//нет элемента с таким ID
	{
		char* buffer = new char [1024];
		std::ifstream ioffset_file ("offset.txt");
		ioffset_file.getline (buffer, 1024);
		unsigned long offset = std::atoi(buffer);
		ioffset_file.close();//нужно записать по смещению

		ObjectAdress obj(offset, filesize, filename);//создаём метаданные
		(*storage)[id] = obj;

		pwrite(fd, data, filesize, offset);
		
		offset += filesize;
		std::ofstream ooffset_file ("offset.txt");
		ooffset_file << offset;
		ooffset_file.close();
	}
	else
	{
		ObjectAdress obj = (*storage)[id];

		if (filesize > obj.get_size())
		{

			char* buffer = new char [1024];
			std::ifstream ioffset_file ("offset.txt");
			ioffset_file.getline (buffer, 1024);
			unsigned long offset = std::atoi(buffer);
			ioffset_file.close();

			obj.set_size(filesize);
			obj.set_offset(offset);
			(*storage)[id] = obj;

			pwrite(fd, data, filesize, offset);

			offset = filesize + offset;
			std::ofstream ooffset_file ("offset.txt");
			ooffset_file << offset;
			ooffset_file.close();
		}
		else
		{
			obj.set_size(filesize);
			(*storage)[id] = obj;

			auto offset = obj.get_offset();

			pwrite(fd, data, filesize, offset);
		}
	}

	ReplyPB reply;
	reply.add_id(id);
	reply.set_success(true);

	size_t reply_length = reply.ByteSize();//записываем длину запроса
	size_t prefix_length = sizeof(reply_length);//вычисляем длину префикса (длины запроса)
	boost::asio::write(sock, boost::asio::buffer(&reply_length, prefix_length));//пишем в сокет длину запроса

	asio::streambuf buf; //создаём буфер
	std::ostream os(&buf); //создаём поток вывода для буфера
	reply.SerializeToOstream(&os);//сериализуем ответ в поток вывода
	boost::asio::write(sock, buf); //пишем в сокет из буфера
}

}
