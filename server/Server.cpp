#include "Server.hpp"
#include "Session.hpp"
#include "PutRequest.hpp"
#include "GetRequest.hpp"
#include "RemoveRequest.hpp"
#include "ListRequest.hpp"

namespace Hammerspace
{
Server::Server (asio::io_service& io_service, unsigned short int port, int cp_num = 100) :
storage (new std::map<unsigned long, ObjectAdress>),
dispatchers (new std::vector<Request*>),
connection_pool (new threadpool::pool(cp_num)),
port(port),
a(io_service, ip::tcp::endpoint(ip::tcp::v4(), port)),//приёмник
sock (io_service)
{
	this->dispatchers->push_back(new PutRequest);
	this->dispatchers->push_back(new GetRequest);
	this->dispatchers->push_back(new RemoveRequest);
	this->dispatchers->push_back(new ListRequest);
}

void Server::run ()
{
	std::cout << "Running on port " << port << ".\n";
	for (;;)
	{
		a.accept(sock);//принимаем в сокет
		std::shared_ptr <Server> server(this);
		auto s = new Session(std::move(sock), server);//создаём объект сессии
		connection_pool->schedule(std::bind(&Session::run, s));//запускаем сессию в треде
	}
}

std::shared_ptr < std::map <unsigned long, ObjectAdress> > Server::get_storage ()
{
	return storage;
}

std::shared_ptr < std::vector<Request*> > Server::get_dispatchers ()
{
	return dispatchers;
}

}
