#include "Session.hpp"

namespace Hammerspace
{
Session::Session(ip::tcp::socket sock, std::shared_ptr <Server> server) :
sock (std::move(sock)),
server (server)
{
}

RequestPB Session::receive_request ()
{
	boost::system::error_code error;
	size_t message_length = 0;
	sock.read_some(asio::buffer(&message_length, sizeof(size_t)), error);//считываем длину сообщения
	if (error)
		throw boost::system::system_error(error);

	char* data = new char[message_length]; //массив для считывания данных из сокета
	sock.read_some(asio::buffer(data, message_length), error);//читаем данные в буфер
	if (error)
		throw boost::system::system_error(error);

	RequestPB request;
	request.ParseFromArray (data, message_length);

	return request;
}

void Session::run()
{
	try
	{
		auto dispatchers = server->get_dispatchers();
		auto storage = server->get_storage();

		for (;;)
		{
			RequestPB request;
			request = receive_request ();

            for (auto it = dispatchers->begin(); it != dispatchers->end(); it++)
                {
			    if((*it)->CanHandle(request))
			    {
			    	(*it)->Handle(std::move(sock), request, storage);
			    }
            }
		}
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception in thread: " << e.what() << "\n";
	}
}
}
