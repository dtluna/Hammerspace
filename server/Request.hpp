#ifndef REQUEST_HPP
#define REQUEST_HPP

#include "functions.hpp"
#include "ObjectAdress.hpp"

namespace Hammerspace
{
class Request
{
	public:
	    virtual bool CanHandle (RequestPB& request) = 0;
		virtual void Handle (ip::tcp::socket sock, RequestPB& request,
				std::shared_ptr < std::map <unsigned long, ObjectAdress> > storage)= 0;
	private:
};
}
#endif // REQUEST_HPP
