#include "ObjectAdress.hpp"

namespace Hammerspace
{

ObjectAdress::ObjectAdress()
{
	offset = 0;
	size = 0;
}

ObjectAdress::ObjectAdress(unsigned long offset, unsigned long size, std::string filename)
{
	this->offset = offset;
	this->size = size;
	this->filename = filename;
}

unsigned long ObjectAdress::get_offset()
{
	return offset;
}
void ObjectAdress::set_offset(unsigned long offset)
{
	this->offset = offset;
}

unsigned long ObjectAdress::get_size()
{
	return size;
}
void ObjectAdress::set_size(unsigned long size)
{
	this->size = size;
}

std::string ObjectAdress::get_filename()
{
	return filename;
}
void ObjectAdress::set_filename(std::string filename)
{
	this->filename = filename;
}

ObjectAdress& ObjectAdress::operator = (const ObjectAdress& other)
{
	this->offset = other.offset;
	this->size = other.size;
	this->filename = other.filename;
	
	return *this;
}
}
