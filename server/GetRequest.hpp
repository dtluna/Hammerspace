#ifndef GETREQUEST_HPP
#define GETREQUEST_HPP

#include "Request.hpp"

namespace Hammerspace
{
class GetRequest : public Request
{
	public:
		bool CanHandle (RequestPB& request);
		void Handle (ip::tcp::socket sock, RequestPB& request,
			std::shared_ptr < std::map <unsigned long, ObjectAdress> > storage);

	protected:
	private:
};
}
#endif // GETREQUEST_HPP