#ifndef OBJECT_ADRESS_HPP
#define OBJECT_ADRESS_HPP

#include <string>

namespace Hammerspace
{

class ObjectAdress
{
private:
	unsigned long offset;
	unsigned long size;
	std::string filename;
public:
	ObjectAdress();
	ObjectAdress(unsigned long offset, unsigned long size, std::string filename);

	unsigned long get_offset();
	void set_offset(unsigned long offset);

	unsigned long get_size();
	void set_size(unsigned long size);

	std::string get_filename();
	void set_filename(std::string filename);

	ObjectAdress& operator = (const ObjectAdress& other);
};

}
#endif // OBJECT_ADRESS_HPP
