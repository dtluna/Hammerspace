#ifndef PUTREQUEST_HPP
#define PUTREQUEST_HPP

#include "Request.hpp"

namespace Hammerspace
{
class PutRequest : public Request
{
	public:
		bool CanHandle (RequestPB& request);
		void Handle (ip::tcp::socket sock, RequestPB& request,
			std::shared_ptr < std::map <unsigned long, ObjectAdress> > storage);

	protected:
	private:
};
}
#endif // PUTREQUEST_HPP
