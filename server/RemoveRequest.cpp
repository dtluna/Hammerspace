#include "RemoveRequest.hpp"

namespace Hammerspace
{

bool RemoveRequest::CanHandle (RequestPB& request)
{
	if(request.HasExtension(RemoveRequestPB::remove_request) )
		return true;
	else return false;
}

void RemoveRequest::Handle (ip::tcp::socket sock, RequestPB& request,
	std::shared_ptr < std::map <unsigned long, ObjectAdress> > storage)
{
	RemoveRequestPB* remove_request = request.MutableExtension(RemoveRequestPB::remove_request);

	auto id = remove_request->id();

	ReplyPB reply;

	auto iter = storage->find(id);
	if (iter == storage->end())//нет элемента с таким ID
	{
		reply.set_success(false);

		size_t reply_length = reply.ByteSize();//записываем длину запроса
		size_t prefix_length = sizeof(reply_length);//вычисляем длину префикса (длины запроса)
		asio::write(sock, asio::buffer(&reply_length, prefix_length));//пишем в сокет длину запроса

		asio::streambuf buf; //создаём буфер
		std::ostream os(&buf); //создаём поток вывода для буфера
		reply.SerializeToOstream(&os);//сериализуем ответ в поток вывода
		asio::write(sock, buf); //пишем в сокет из буфера
	}
	else
	{
		storage->erase(id);

		reply.set_success(true);

		size_t reply_length = reply.ByteSize();//записываем длину запроса
		size_t prefix_length = sizeof(reply_length);//вычисляем длину префикса (длины запроса)
		asio::write(sock, asio::buffer(&reply_length, prefix_length));//пишем в сокет длину запроса

		asio::streambuf buf; //создаём буфер
		std::ostream os(&buf); //создаём поток вывода для буфера
		reply.SerializeToOstream(&os);//сериализуем ответ в поток вывода
		asio::write(sock, buf); //пишем в сокет из буфера
	}
}

}
