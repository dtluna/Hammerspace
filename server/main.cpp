#include "functions.hpp"
#include "Server.hpp"

using namespace Hammerspace;

const int max_length = 1024;

int main(int argc, char* argv[])
{

	// Verify that the version of the library that we linked against is
	// compatible with the version of the headers we compiled against
	GOOGLE_PROTOBUF_VERIFY_VERSION;
	try
	{
		po::options_description desc("Allowed options");
		desc.add_options()
		("help,h", "produce help message")
		("port,p", po::value< unsigned short >(),
			"listen to port (default port is 45000)")
		("conn-pool,c", po::value< unsigned short >(),
			"set a number of threads for connection (default number is 1000)")
		;

		po::positional_options_description p;
		p.add("port", -1);

		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).
			options(desc).positional(p).run(), vm);
		po::notify(vm);

		if (vm.count("help"))
		{
			help(desc);
		}

		unsigned short int cp_num;
		if (vm.count("conn-pool"))
			cp_num = vm["conn-pool"].as<unsigned short int>();
		else
			cp_num = 100;

		unsigned short int port;
		if (vm.count("port"))
			port = vm["port"].as<unsigned short int>();
		else
			port = 45000;

        boost::asio::io_service io_service;
		Server server (io_service, port, cp_num);//создаём сервер
		server.run();//запускаем сервер

	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}
